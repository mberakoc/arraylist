#include "lib/arraylist.h"

int main() {
    ArrayList my_array_list = _Create();
    for (int i = 1; i < 10; ++i)
        add(&my_array_list, i);
    set(&my_array_list, 1, 23);
    _remove(&my_array_list, 4);
    _remove_with_index(&my_array_list, 6);
    printf("Element in 1st index is %d.\n", get(my_array_list, 1));
    ArrayList filled_array_list = _CreateWithGivenArray((int[]){2, 3, 5, 8, 13}, 5);
    ArrayList duplicate_array_list = _CreateWithGivenList(filled_array_list);
    _print(duplicate_array_list);
    printf("Is filled list equal to duplicate list? %s\n",
            equals(&filled_array_list, &duplicate_array_list) ? "Yes" : "No");
    merge(&my_array_list, filled_array_list, 7);
    puts("Filled list is cleared.");
    clear(&filled_array_list);
    printf("Is filled list empty? %s\n", is_empty(filled_array_list) ? "Yes" : "No");
    _print(my_array_list);
    printf("First index of 8: %d\n", index_of(my_array_list, 8, FIRST));
    printf("Last index of 8: %d\n", index_of(my_array_list, 8, LAST));
    replace(&my_array_list, 8, 12);
    puts("All 8 values replaced by 12.");
    _print(my_array_list);
    _print(sublist(my_array_list, 5, 11));
    sort(&my_array_list);
    puts("ArrayList is sorted.");
    _print(my_array_list);
    printf("Does list contains 13? %s\n", contains(my_array_list, 13) ? "Yes" : "No");
    printf("Does list contains 98? %s\n", contains(my_array_list, 98) ? "Yes" : "No");
    printf("Index of 53: %d\n", index_of(my_array_list, 53, LAST));
    trim_to_size(&my_array_list);
    printf("Capacity of trimmed ArrayList: %d\n", my_array_list.capacity);
    printf("8th element of ArrayList: %d\n", to_array(my_array_list)[7]);
    return EXIT_SUCCESS;
}

