#ifndef ARRAYLIST_H
#define ARRAYLIST_H

#include <stdlib.h>
#include <stdio.h>
#define DEFAULT_CAPACITY 10

typedef enum boolean boolean;
typedef enum MergeIndex MergeIndex;
typedef enum IndexPosition IndexPosition;
typedef struct ArrayList ArrayList;
typedef ArrayList * ArrayListPtr;
typedef struct Functions Functions;

enum boolean
{
   false, true
};

enum MergeIndex
{
    AUTO = -1
};

enum IndexPosition
{
    FIRST, LAST
};

struct ArrayList
{
    int *array;
    int capacity;
    int size;
    boolean is_sorted;
};

struct Functions
{
    ArrayList (*_Create)(void);
    ArrayList (*_CreateWithGivenArray)(const int[],int);
    ArrayList (*_CreateWithGivenList)(const ArrayList);
    void (*_adjust_size)(ArrayListPtr);
    void (*add)(ArrayListPtr,int);
    void (*add_using_array)(ArrayListPtr,int[],int);
    void (*set)(ArrayListPtr,int,int);
    void (*_remove)(ArrayListPtr,int);
    void (*_remove_using_array)(ArrayListPtr,int[],int);
    void (*_remove_with_index)(ArrayListPtr,int);
    void (*_remove_with_index_array)(ArrayListPtr,int[],int);
    void (*merge)(ArrayListPtr,ArrayList,int);
    void (*clear)(ArrayListPtr);
    boolean (*is_empty)(ArrayList);
    int (*__Compare)(const void *,const void *);
    void (*sort)(ArrayListPtr);
    boolean (*contains)(ArrayList,int);
    int (*index_of)(ArrayList,int,IndexPosition);
    void (*replace)(ArrayListPtr,int,int);
    boolean (*equals)(ArrayListPtr,ArrayListPtr);
    void (*trim_to_size)(ArrayListPtr);
    int * (*to_array)(ArrayList);
    ArrayList (*sublist)(ArrayList,int,int);
    void (*_print)(ArrayList);
};

Functions __functions;

/**
 * Creates an empty ArrayList with default capacity.
 * @return ArrayList New ArrayList struct
 */
ArrayList _Create()
{
    int *array = calloc(sizeof(int), DEFAULT_CAPACITY);
    return (ArrayList) {.array=array, .capacity=DEFAULT_CAPACITY, .size=0, .is_sorted=false};
}


void add(ArrayListPtr array_list_ptr, int value);
/**
 * Creates an ArrayList using given array
 * @param array Given array to create an Array List
 * @param length Length of the array since arrays shrinks as parameters
 * @return ArrayList New ArrayList struct
 */
ArrayList _CreateWithGivenArray(const int array[], int length)
{
    ArrayList array_list = _Create();
    for (int i = 0; i < length; ++i)
        add(&array_list, array[i]);
    return array_list;
}

void merge(ArrayListPtr array_list_ptr, ArrayList merged_array_list, int merge_index);
/**
 * Creates an ArrayList using the given ArrayList
 * @brief This functions corresponds to clone() method in Java.
 * @param array_list Given ArrayList to create a new ArrayList
 * @return ArrayList New ArrayList struct
 */
ArrayList _CreateWithGivenList(const ArrayList array_list)
{
    ArrayList new_array_list = _Create();
    merge(&new_array_list, array_list, 0);
    return new_array_list;
}

/**
 * @brief Triggered whenever the size of an Arraylist is equal to its capacity
 * Increases capacity when ArrayList is full
 * @param array_list_ptr A pointer to an ArrayList which is full in capacity
 */
void _adjust_size(ArrayListPtr array_list_ptr)
{
    ArrayList array_list = *array_list_ptr;
    int *new_array = calloc(sizeof(int), array_list.capacity * 2);
    array_list_ptr->capacity = array_list.capacity * 2;
    for (int i = 0; i < array_list.size; ++i)
        new_array[i] = array_list.array[i];
    array_list_ptr->array = new_array;
}

/**
 * Adds a new value to the end of ArrayList
 * @param array_list_ptr ArrayList which is a new element is to be inserted
 * @param value Integer value to be added
 */
void add(ArrayListPtr array_list_ptr, int value)
{
    ArrayList array_list = *array_list_ptr;
    if (array_list.size == array_list.capacity)
        _adjust_size(array_list_ptr);
    array_list_ptr->is_sorted = false;
    array_list_ptr->array[array_list_ptr->size++] = value;
}

/**
 * Same as add. Only difference is it uses an integer array as a value
 * @param array_list_ptr
 * @param array Integer array to be added
 * @param length Length of the array
 */
void add_using_array(ArrayListPtr array_list_ptr, int array[], int length)
{
    for (int i = 0; i < length; ++i)
        add(array_list_ptr, array[i]);
}

/**
 * @brief Adds a new value to a specific index of an ArrayList
 * @param array_list_ptr ArrayList which is a new element is to be inserted
 * @param index Specific index where value is to be added
 * @param value Integer value to be added.
 */
void set(ArrayListPtr array_list_ptr, int index, int value)
{
    ArrayList array_list = *array_list_ptr;
    if (array_list.size == array_list.capacity)
        _adjust_size(array_list_ptr);
    for (int i = array_list_ptr->size++ - 1; i >= index; --i)
        array_list_ptr->array[i + 1] = array_list_ptr->array[i];
    array_list_ptr->is_sorted = false;
    array_list_ptr->array[index] = value;
}

/**
 * @brief Removes an element from ArrayList with the given value
 * @param array_list_ptr ArrayList which is an element is to be removed
 * @param value Integer value to be removed
 */
void _remove(ArrayListPtr array_list_ptr, int value)
{
    boolean is_remove_process_active = false;
    for (int i = 0; i < array_list_ptr->size; ++i)
    {
        if (array_list_ptr->array[i] == value)
            is_remove_process_active = true;
        if (is_remove_process_active)
            array_list_ptr->array[i] = array_list_ptr->array[i + 1];
    }
    --array_list_ptr->size;
    array_list_ptr->is_sorted = false;
}

/**
 * Same as _remove. It uses an array for removal
 * @param array_list_ptr
 * @param array
 * @param length
 */
void _remove_using_array(ArrayListPtr array_list_ptr, int array[], int length)
{
    for (int i = 0; i < length; ++i)
        _remove(array_list_ptr, array[i]);
}

/**
 * Same as remove only difference is it uses index instead of value
 * @param array_list_ptr
 * @param index
 */
void _remove_with_index(ArrayListPtr array_list_ptr, int index)
{
    _remove(array_list_ptr, array_list_ptr->array[index]);
}

/**
 * Same as _remove_with_index only it uses an index array instead of single index value
 * @param array_list_ptr
 * @param index_array
 * @param length
 */
void _remove_with_index_array(ArrayListPtr array_list_ptr, int index_array[], int length)
{
    for (int i = 0; i < length; ++i)
        _remove_with_index(array_list_ptr, index_array[i]);
}

/**
 * Gets the value at given index
 * @param array_list
 * @param index
 * @return Integer value in given index of ArrayList
 */
int get(ArrayList array_list, int index)
{
    return array_list.array[index];
}

/**
 * Merges two Arraylist updating the first one
 * @param array_list_ptr Base ArrayList to hold the merge result
 * @param merged_array_list Arraylist to be merged to the base ArrayList
 * @param merge_index Index that indicates from where the merge will begin with respect to the first ArrayList
 */
void merge(ArrayListPtr array_list_ptr, ArrayList merged_array_list, int merge_index)
{
    if (merge_index == AUTO)
        for (int i = 0; i < merged_array_list.size; ++i)
            add(array_list_ptr, merged_array_list.array[i]);
    else
    {
        for (int i = 0; i < merged_array_list.size; ++i)
            set(array_list_ptr, merge_index++, merged_array_list.array[i]);
    }
    array_list_ptr->is_sorted = false;
}

/**
 * Clears an ArrayList removing all elements inside it
 * @param array_list_ptr
 */
void clear(ArrayListPtr array_list_ptr)
{
    array_list_ptr->array = (int[]){};
    array_list_ptr->size = 0;
    array_list_ptr->capacity = DEFAULT_CAPACITY;
    array_list_ptr->is_sorted = false;
}

/**
 * Detects if an ArrayList is empty -Has no elements-
 * @param array_list
 * @return A boolean value which is true if it is empty
 */
boolean is_empty(ArrayList array_list)
{
    return array_list.size == 0;
}

/**
 * Comparator for sort and contains functions
 * Compares by ascending order
 * @param a
 * @param b
 * @return
 */
int __Compare(const void *a, const void *b)
{
    return *(int *) a - *(int *)b;
}

/**
 * Sorts a given ArrayList using Quick Sort.
 * @param array_list_ptr ArrayList pointer to be sorted
 */
void sort(ArrayListPtr array_list_ptr)
{
    ArrayList array_list = *array_list_ptr;
    int old_value = -1, new_value;
    boolean is_already_sorted = true;
    for (int i = 0; i < array_list.size; ++i)
    {
        new_value = array_list.array[i];
        if (new_value < old_value)
            is_already_sorted = false;
        old_value = new_value;
    }
    if (is_already_sorted == true)
    {
        array_list_ptr->is_sorted = true;
        return;
    }
    qsort(array_list.array, array_list.size, sizeof(array_list.array[0]), __Compare);
    array_list_ptr->is_sorted = true;
}

/**
 * Detects if an ArrayList contains the given value
 * @param array_list
 * @param value Value to be tested
 * @return A boolean that indicates whether ArrayList contains the value or not
 */
boolean contains(ArrayList array_list, int value)
{
    if (array_list.is_sorted == true)
        return bsearch(&value, array_list.array, array_list.size,
                sizeof(array_list.array[0]), __Compare) != NULL ? true : false;
    else
    {
        for (int i = 0; i < array_list.size; ++i)
            if (array_list.array[i] == value)
                return true;
    }
    return false;
}

/**
 * Finds the index of the given value using IndexPosition
 * @param array_list
 * @param value
 * @param index_position If FIRST starts from left, else if LAST starts from right to search the given value
 * @return Index for the given value
 */
int index_of(ArrayList array_list, int value, IndexPosition index_position)
{
    if (array_list.is_sorted)
        if (contains(array_list, value) != true)
            return -1;
    int i = index_position == FIRST ? 0 : array_list.size - 1;
    while (index_position == FIRST ? i < array_list.size : i >= 0)
    {
        if (array_list.array[i] == value)
            return i;
        i = index_position == FIRST ? ++i : --i;
    }
    return -1;
}

/**
 * Replaces all old values with the new values
 * @param array_list_ptr
 * @param old_value
 * @param new_value
 */
void replace(ArrayListPtr array_list_ptr, int old_value, int new_value)
{
    for (int i = 0; i < array_list_ptr->size; ++i)
        if (array_list_ptr->array[i] == old_value)
            array_list_ptr->array[i] = new_value;
}

/**
 * Detects if two ArrayLists have same size and same elements
 * @param first_array_list_ptr
 * @param second_array_list_ptr
 * @return
 */
boolean equals(ArrayListPtr first_array_list_ptr, ArrayListPtr second_array_list_ptr)
{
    if (first_array_list_ptr->size != second_array_list_ptr->size)
        return false;
    sort(first_array_list_ptr);
    sort(second_array_list_ptr);
    for (int i = 0; i < first_array_list_ptr->size; ++i)
        if (first_array_list_ptr->array[i] != second_array_list_ptr->array[i])
            return false;
    return true;
}

/**
 * Reduces the capacity of an ArrayList to its current size
 * @brief The function is very useful for reducing the program heap space
 * @param array_list_ptr
 */
void trim_to_size(ArrayListPtr array_list_ptr)
{
    int *array = calloc(sizeof(int), array_list_ptr->size);
    for (int i = 0; i < array_list_ptr->size; ++i)
        array[i] = array_list_ptr->array[i];
    array_list_ptr->array = array;
    array_list_ptr->capacity = array_list_ptr->size;
}

/**
 * Returns the array form of the ArrayList
 * @param array_list
 * @return An array of integers
 */
int * to_array(ArrayList array_list)
{
    return array_list.array;
}

/**
 * Creates a new ArrayList from the given ArrayList using given indices
 * @param array_list ArrayList from which sublist will be created
 * @param begin_index Shows where sublist starts -Included-
 * @param end_index Shows where sublist ends -Not included-
 * @return A new sublist satisfies given conditions
 */
ArrayList sublist(ArrayList array_list, int begin_index, int end_index)
{
    int *array = calloc(sizeof(int), end_index - begin_index);
    for (int i = begin_index; i < end_index; ++i)
        array[i - begin_index] = array_list.array[i];
    ArrayList new_array_list = _CreateWithGivenArray(array, end_index - begin_index);
    return new_array_list;
}

/**
 * Prints an ArrayList by its elements
 * @param array_list
 */
void _print(ArrayList array_list)
{
    printf("[");
    for (int i = 0; i < array_list.size; ++i)
        printf(i == array_list.size - 1 ? "%d" : "%d, ", array_list.array[i]);
    printf("]\n");
}

/**
 * A function to initialise __functions variable
 */
void __init__()
{
    __functions = (Functions) {._remove_with_index=_remove_with_index, ._remove=_remove, .add=add,
                               .sublist=sublist, .to_array=to_array, .trim_to_size=trim_to_size,
                               .equals=equals, .sort=sort, .replace=replace, .index_of=index_of,
                               .contains=contains, .merge=merge, .is_empty=is_empty, .clear=clear,
                               ._adjust_size=_adjust_size, .__Compare=__Compare, ._Create=_Create,
                               ._CreateWithGivenArray=_CreateWithGivenArray,
                               ._CreateWithGivenList=_CreateWithGivenList, ._print=_print,
                               ._remove_using_array=_remove_using_array,
                               ._remove_with_index_array=_remove_with_index_array,
                               .add_using_array=add_using_array, .set=set};
}

#endif