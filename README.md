# [ArrayList](https://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html) <br>[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-ffd800.svg)](https://www.gnu.org/licenses/gpl-3.0) [![Generic badge](https://img.shields.io/badge/version-v1.0.3-blueviolet.svg)](https://shields.io/) [![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-brightgreen.svg)](https://GitHub.com/Naereen/StrapDown.js/graphs/commit-activity)
An implementation of ArrayList class using C.
* __Easy To Learn:__ Library is designed to provide a smooth learning curve. It has a very clear naming for variables and functions.
* __Fully-Implemented:__ Nearly every method of ArrayList class of Java -except those about OOP- is implemented in a very neat and robust way. 
* __Just One Header:__ Using only arraylist.h you can access all functionality of ArrayList.
# Documentation
ArrayList is documented via Doxygen.

Check out the [Getting Started](https://gitlab.com/mberakoc/arraylist/-/wikis/home) for quick review.

## Licence 
Symbol is [GNU Licensed.](https://gitlab.com/mberakoc/arraylist/blob/master/LICENSE)